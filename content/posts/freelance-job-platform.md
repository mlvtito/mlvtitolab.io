+++ 
date = "2019-02-08"
title = "Freelance : les plateformes de mise en relation"
tags = []
categories = []
+++

Je suis en mission et j'ai toujours été en mission, je n'ai jamais eu besoin de faire de recherche poussée (ceci dit mon aventure en tant qu'indépendant est ecore toute fraiche). Toutefois, pour ne pas être démuni quand j'en aurai besoin, j'ai décidé de faire le tour des plateformes de mise en relation qui existent.

A force de vivre dans le domaine du développement et de cotoyer des indépendants, mon esprit a pu sortir quelques noms sans réfléchir; mais, après une petite recherche, je ne m'attendai pas à obtenir une liste aussi longue.

Pour le moment, la liste est brut de fonderie. Au fur et à mesure de mes explorations avec chacun de ces sites, je complèterai avec le mode de fonctionnement de chacun et mes impressions.

* https://welovedevs.com
* https://www.yoss.com
* https://www.coworkees.com/
* http://freelance.viadeo.com/fr/freelance
* https://www.reseau-freelances.com/
* https://skillvalue.com/coders/fr/freelance/
* https://www.xxe.fr/
* https://www.talent.io/
* https://www.bubbleting.com/
* https://greaaat.com/
* https://www.freelance.com/
* http://www.missioneo.fr/
* https://www.airjob.fr/
* https://www.turnover-it.com/
* http://www.humaniance.com/ (pas sûr qu'il soit encore d'actualité)
* http://www.itprofil.com/ (la recherche ne donne rien)
* https://www.codeur.com/
* https://www.404works.com/
* https://www.twago.fr/
* https://www.bubbleting.com/
* https://comet.co/
* https://cremedelacreme.io/
* https://www.malt.com/
* https://www.freelancerepublik.com/
* https://www.littlebigconnection.com/

Un peu différent :

* https://5euros.com/



Pour les plus internationaux : 

* https://hire.codementor.io/
* https://www.toptal.com
* https://remote.com/
* https://www.fiverr.com/
* https://www.upwork.com/
* https://www.freelancer.com/


