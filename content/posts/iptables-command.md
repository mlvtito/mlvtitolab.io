+++ 
date = "2012-02-20"
title = "Mémo : Commandes iptables"
tags = []
categories = []
+++


Juste un petit mémo pour me souvenir des paramètres de la commande iptables.

Lister les règles en usage
--------------------------

```bash
iptables -v -L
```

Vider les règles
----------------

```bash
iptables -t filter -F
iptables -t filter -X # vider les règles personnelles
```

Par ailleurs, le wiki Fedora sur le sujet est plutôt bien fait : http://doc.fedora-fr.org/wiki/Parefeu_-_firewall_-_netfilter_-_iptables
