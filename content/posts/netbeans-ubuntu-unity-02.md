+++ 
date = "2015-05-29"
title = "Netbeans & Unity : ça marche presque tout seul"
tags = []
categories = []
+++

Dans un précédent article, je partageai un plugin Netbeans pour profiter du menu global du bureau Unity d’Ubuntu. Et bien chers amis, je vous le dis : n’installer plus ce plugin.

Ne vous inquiétez pas, je ne viens pas vous annoncer une nouvelle faille de sécurité dans ce plugin (c’est à la mode en ce moment). C’est juste que son utilisation n’est plus nécessaire car les dépôts officiels d’Ubuntu contiennent la librairie Ayatana qui s’appliquera à tout vos programme Java. Et oui, vous m’avez bien lu : « à tous vos programme Java ».

Si sur votre poste, on peut trouvez un browser Cassandra écrit en Java, un serveur bouchon SMTP écrit en Java, tous profiteront du menu centralisé et surtout de la recherche dans les menus.

Elle est pas belle la vie ? Comment on fait maintenant ?

Et bien rien si vous êtes passé à Ubuntu 15.04, c’est actif automatiquement.

Et pour les autres, restez concentré, ça va aller vite :

```bash
sudo add-apt-repository ppa:danjaredg/jayatana
sudo apt-get update
sudo apt-get install jayatana
```

Bien entendu, ça fonctionne aussi pour les non développeurs, ça fonctionne aussi avec jDownloader.

