+++ 
date = "2016-10-14"
title = "Développeur Frontend derrière un proxy"
tags = []
categories = []
+++

Je viens du monde Java et ce qui me manque le plus lorsque je fais du développement frontend, c’est bien la maturité de l’écosystème. Et je le ressens beaucoup quand j’essaie de travailler derrière un proxy et que je me rends compte que chaque outil qui participe au buid de mon application utilise un paramétrage différent.

C’est en tentant de reprendre le squelette d’application Aurelia en Typescript que j’ai cherché tous les paramétrages ci-dessous.

== Unix

Tout d’abord, il faut configurer le proxy système. Sous Linux, il suffit de déclarer les variables ci-dessous :

```
export http_proxy=http://proxy.host:8080/
export https_proxy=http://proxy.host:8080/
```

== NPM

Pour la commande NPM, il suffit de créer un fichier « .npmrc » à la racine du répertoire utilisateur :

```
proxy=http://proxy.host:8080/
```

== Typings

Il suffit de créer un fichier « .typingsrc » à la racine du répertoire utilisateur :

```
proxy=http://proxy.host:8080/
```

== JSPM

Pour JSPM, si vous avez bien suivi le paramétrage Unix ci-dessus, ça devrait fonctionner (sachant que le piège se trouve dans au niveau du proxy HTTPS qui doit utiliser le protocole « http » simple).
