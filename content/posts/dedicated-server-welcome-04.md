+++ 
date = "2016-11-05"
title = "Bien recevoir son serveur dédié (suite : c’est jamais fini)"
tags = []
categories = []
+++

Je reviens sur la sécurisation de mon petit serveur.Je dois avouer que depuis le changement de port SSH, je suis plutôt tranquille. Je vais tout de même aller plus loin en réduisant la liste des users autorisés à se connecter via SSH.

Et comme toujours, c’est relativement simple. On édite le fichier /etc/ssh/sshd_config et on ajoute une ligne du type :

```apache
AllowUsers moncompte
```


