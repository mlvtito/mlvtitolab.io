+++ 
date = "2012-02-14"
title = "Fedora 16 : interfaces réseaux, iptables et service"
tags = []
categories = []
+++

Suite à mon installation Oracle 10g sur Fedora. J’ai été perturbé par le fait que les interfaces réseaux ne se configuraient pas automatiquement au démarrage de ma machine virtuelle.

Je ne sais pas pourquoi mais ça semble être quelque chose de normal. Pour rendre la chose automatique, voici les commandes à passer :

```bash	
service network restart
chkconfig network on
```

Et tout fonctionne comme ça. Par exemple, pour supprimer les règles iptables (qui sont très bien sur un environnement de production mais génant pour mes tests avec ma machine virtuelle), il suffit de taper :

```bash	
service iptables stop
chkconfig iptables off
```

En y regardant de plus près, dans les faits, chkconfig est une surcouche aux gestionnaires de service xinetd (successeur de feu inetd). On peut par exemple lister tous les services avec la commande :

```bash	
chkconfig --list
```

Pour le moment, je n’ai pas encore trouvé d’équivalent sous Ubuntu. Il faut que je creuse les script Upstart à l’occasion.

