+++ 
date = "2022-01-07"
title = "Trouver le processus qui écoute sur un port donnée"
description = "Un résumé des commandes permettant de trouver le processus qui écoute sur un port "
tags = ["process", "port", "windows","linux"]
categories = ["Operating System"]
+++

J'ai été récemment confronté à un programme qui refusait de démarrer car le port d'écoute sur lequel il essayait de se brancher était déjà occupé.

Retrouver le processus qui monopolise un port n'est pas une tache que je mène régulièrement. C'est même plutôt rare.

Je résume donc les commandes qui permettent de le faire ici.

### Sous Windows

Tout d'abord, on recherche l'identifiant du processus

```
Get-Process -Id (Get-NetTCPConnection -LocalPort 6000).OwningProcess
```

Sur la commande ci-dessus, j'utilise le port 6000 (et oui, c'est un serveur X11 qui refusait de démarrer). Vous allez alors avoir une réponse qui ressemble à ceci:

```
PS C:\> Get-Process -Id (Get-NetTCPConnection -LocalPort 6000).OwningProcess

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    513      38     8844      14460              4012   0 svchost
```

Dans mon cas, `svchost` est un espèce de processus chapeau à d'autres. Donc la recherche ne s’arrête pas là.

Voici un moyen de lister les services qu'on pourra retrouver via l'ID du processus.

```
tasklist /svc
```

### Sous Linux

Le plus simple est la commande `lsof`qui liste les fichiers ouverts. Rappelez vous que tout est fichier sous Linux.

```
prompt>sudo lsof -s TCP:LISTEN -i :80
COMMAND   PID     USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
apache2  8350     root    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2  8352 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2  8353 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2  8354 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2  8355 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2  8356 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2 37953 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2 37980 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)
apache2 37981 www-data    4u  IPv6  79877      0t0  TCP *:http (LISTEN)

```

OMG ! J'ai un process Apache sur le port 80 !

Voilà de quoi connaître un peut mieux votre système et résoudre plus rapidement vos problèmes de gestion de ports.