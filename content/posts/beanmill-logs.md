+++ 
date = "2013-04-22"
title = "BeanMill ou une autre vision des logs"
tags = []
categories = []
+++

Ah les fichiers de logs, les rotations de fichiers, les commandes tail sous Windows. Un fichier de log, c’est bien sur un serveur mais sur le poste du développeur, c’est tout bonnement impraticable. Et c’est en rouvrant mon Netbeans préféré que j’ai découvert le plugin BeanMill.

Je n’ai pas compris du premier coup mais c’est en fait très simple. Lorsque le plugin est installé, l’IDE se comporte alors comme un serveur de log. Finalement, il n’y a plus qu’à modifier la configuration des logs sur l’application que nous sommes en train de façonner. Par exemple, si il s’agit d’une configuration log4j, ajouter l’appender suivant au rootLogger pour voir la magie opérer :

```properties
# Log Event appender
log4j.appender.sockets=org.apache.log4j.net.SocketAppender
log4j.appender.sockets.remoteHost=localhost
log4j.appender.sockets.port=4445
log4j.appender.sockets.locationInfo=true
 
log4j.rootLogger=INFO, sockets
```

Dorénavant, l’onglet BeanMill présentera les évènements de log et nous pouvons les filtrer en fonction du contenu de chaque message, nous pouvons affecter des niveaux de trace différents en fonction des packages et le tout dans un rendu coloré qui saute aux yeux.

![](../../assets/images/beanmill-screenshot.png)

Je vous préviens, l’utilisation de ce plugin est fortement addictif mais sans effet secondaire sur la santé.
