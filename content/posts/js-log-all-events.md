+++ 
date = "2019-01-15"
title = "The naive way to debug Javascript events"
tags = []
categories = []
+++

Si vous avez besoin un jour de voir tous les événements qui passent dans votre navigateur (enfin tous ceux qui remontent jusqu'à la fenêtre), voici ce qu'il vous faut : 


```javascript
Object.keys(window).forEach(key => {
    if (/^on/.test(key)) {
        window.addEventListener(key.slice(2), event => {
            console.log(event);
        });
    }
});
```

C'est une expression régulière donc vous pouvez l'adapter pour prendre en compte une liste détéerminé d'événement :

```javascript
Object.keys(window).forEach(key => {
    if (/^on(key|mouse)/.test(key)) {
        window.addEventListener(key.slice(2), event => {
            console.log(event);
        });
    }
});

```

Ou, au contraire, vous pouvez ne pas prendre en compte certains évènements :

```javascript
Object.keys(window).forEach(key => {
    if (/^on(?!message)/.test(key)) {
        window.addEventListener(key.slice(2), event => {
            console.log(event);
        });
    }
});

```

Et n'oubliez pas de tester votre expression avec les bons outils : https://www.regextester.com/

Cette astuce a été fournie par Ryan P. C. McQuen sur Stackoverflow : https://stackoverflow.com/questions/27321672/listen-for-all-events-in-javascript.


