+++ 
date = "2022-04-19"
title = "Copier un fichier dans un container podman"
description = "Dans ce post, je donne la commande pour copier un fichier depuis le serveur host dans un container podman. Très utile pour, par exemple, importer une base de données."
tags = ["container", "podman", "fichier", "linux"]
categories = ["Containers"]
+++

Depuis que j'ai découvert [podman](https://podman.io/) et sa faculté à démarrer des containers rootless et son intégration facile avec [systemd](https://systemd.io/), je n'utilise plus que ça pour démarrer mes bases de données.

En revanche, quand on parle des base de données, on parle souvent aussi de dump. Il faut donc bien avoir les commandes sous la main. C'est pour cela que je les partage ici.

Dans les commandes ci-dessous, j'évoque des commandes `psql` parce que je travaille beaucoup avec [Postgres](https://www.postgresql.org/). Mais c'est sûrement adaptable à votre base de données.

### Installer le client sur le serveur host

Bon qu'on se le dise tout de suite, ne faites pas ça sur votre serveur de production. En revanche, sur votre poste de développeur, c'est le plus simple à faire.

```
sudo apt install postgresql-client
zcat 20220419_db_dump.sql.gz | psql -h localhost -p db-port-on-podman -U db-user db-name
```

Et voilà une méthode simple qui fonctionne avec un container isolé dont le port est exposé sur la machine host.

### La copie de fichier

Si le port de votre container, la seconde méthode la plus simple est de copier le dump dans le container directement et de lancer l'import dans le container lui-même.

```
podman cp 20220419_db_dump.sql.gz container-name:/
podman exec -it container-name bash
zcat 20220419_db_dump.sql.gz | psql -U db-user db-name
```

Ici, nous copions notre fichier dump `20220419_db_dump.sql.gz` à la racine dans mon container que j'ai judicieusement nommé `container-name`. Ensuite nous démarrons un `bash` dans ce même container pour pouvoir lancer notre commande `psql` d'import des données.