+++ 
date = "2022-11-03"
title = "Gérer les exceptions avec JUnit"
description = "Voici un snippet rapide pour gérer les exception en JUnit"
tags = ["java", "junit", "unit test", "test"]
categories = ["Java"]
+++

J'écris cet article après avoir butté sur la gestion d'une exception dans un test unitaire; En effet, je n'avais pas vu le changement entre JUnit 4 et 5.

### JUnit 4

Je ne vais pas faire le tour de toutes les possibilités, voici juste la manière que je trouve la plus simple et directe de vérifier une exception.

```
@Test(expected = RuntimeException.class)
public void should_ThrowException_while_DoingMyThing() {
  var service = new MyService();
  service.doTheThingThatThowTheException();
}
```

Il existe également une autre méthode qui offre plus de controle sur l'exception, en utilisant des `@Rule` que je ne décris pas ici. En effet, je n'en ai jamais eu besoin et je trouve cette syntaxe trop verbeuse.

### JUnit 5

Avec JUnit 5, je trouve cela moins direct, mais nous gagnons en finesse sur les vérifications que nous pouvons faire sur l'exception qui est levée.

```
@Test
public void should_ThrowException_while_DoingMyThing() {
  RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> {
    var service = new MyService();
    service.doTheThingThatThowTheException();
  }

  assertThat(thrown.getMessage()).isEqualTo("Fake exception message");
}
```

On obtient un controlle total sur l'exception qui est levée et elle n'apparait plus dans les logs comme avant. La syntaxe reste tout de même assez concise. Bref j'aime bien.


