+++ 
date = "2012-12-10"
title = "Velocity, si c’est pas null"
tags = []
categories = []
+++

Si au vu du titre, vous vous attendiez à trouver un jugement sur le langage de template Velocity, passez votre chemin. Il s’agit juste d’un petit rappel pour moi même pour avoir le test absolu pour vérifier si une variable est null (non définie) ou vide (chaîne de caractère vide) :

```velocity
#if($car && $car == "")
```

Source : http://www.mail-archive.com/velocity-user@jakarta.apache.org/msg13790.html


