+++ 
date = "2012-02-11"
title = "Se préparer une base Oracle 10g sur une Fedora 16"
tags = []
categories = []
+++

Voici mon pense-bête pour installer une base de données Oracle 10.2.0.1 sur une Fedora 16. Opération que j’ai brillamment exécutée dans une machine virtuelle Virtualbox.

Cette installation va me permettre de pouvoir effectuer des tests (de connexion ou autre comportements hasardeux inhérent au métier) dans des conditions plus proche de ce que je rencontre en entreprise.

D’abord, récupérer le socle nécessaire :

* le DVD d’installation de Fedora 16 ici par exemple : http://fr2.rpmfind.net/linux/fedora/linux/releases/16/Fedora/x86_64/iso/Fedora-16-x86_64-DVD.iso
* et la base de données bien sûr : http://www.oracle.com/technetwork/database/10201linx8664soft-092456.html

On notera que mon système est en 64 bits et que j’aime ça (et mes 8 Go (vivement les 16) aussi). Si votre CPU ne le supporte pas (ce qui devient rare), il faudra utiliser les versions 32 bits correspondantes.

On notera aussi qu’il vous faudra créer un compte sur le site d’Oracle pour télécharger la base de données. C’est gratuit et pour le moment, je ne suis pas trop pollué par leurs mails.

Je vous laisse ensuite le soin d’installer Fedora. Pour ma part, j’ai installé une version minimale.

Avant de commencer, il y a quelques packages à installer :

* Tout d’abord, je me donne un accès SSH :

```bash	
yum install openssh*
```

* Ensuite, j’installe quelques packages 32 bits nécessaires pour l’installeur d’Oracle qui curieusement fonctionne avec une JVM 32 bits :

```bash	
yum install libXp.i686
yum install libXt.i686
yum install libXtst.i686
```

* Enfin un package pour permettre de rediriger les applications graphique (comme l’installeur d’Oracle) vers son propre affichage sans:

```bash	
yum install xauth
```

Connectez vous à votre nouvelle machine avec l’option de transfert d’affichage :

```bash	
ssh -X user@hostname
```

Ensuite, les principales opérations à suivre sont décrites ici : http://www.oracle-base.com/articles/11g/OracleDB11gR2InstallationOnFedora16.php.

Ce site est génial, il présente de manière très clair les installations de différentes versions de base Oracle sur différente version de Linux.

Toutefois, une erreur va se produire au moment de faire des vérifications, une fenêtre va apparaître avec une insulte du genre :

```	
ORA-27125: unable to create shared memory segment
```

Dans ce cas, pas de panique, laisser cette fenêtre ouverte, appliquer la correction suivante et cliquer sur « Retry » :

```bash
cd ${ORACLE_HOME}/bin
mv oracle oracle.bin
 
cat >oracle <<"EOF"
#!/bin/bash
 
export DISABLE_HUGETLBFS=1
exec ${ORACLE_HOME}/bin/oracle.bin $@
EOF
 
chmod +x oracle
```

Et voilà, vous avez à disposition une base de données 10g pour vos tests

