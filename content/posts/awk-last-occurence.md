+++ 
date = "2017-02-20"
title = "Dernière occurence d’une chaine en awk"
tags = []
categories = []
+++

Voilà une fonction pour awk qui m’a bien dépanné :

```awk
function rindex(str,c)
{
  return match(str,"\\" c "[^" c "]*$")? RSTART : 0
}
```

Avec ça, je récupère la dernière occurence d’une chaine de caractères (comme lastIndexOf en Java).

