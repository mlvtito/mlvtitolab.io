+++ 
date = "2012-02-27"
title = "Gitblit : premier commit"
tags = []
categories = []
+++

Gitblit est dans la place et j’en suis pour le moment très satisfait. Il faut dire qu’avec trois dépôts à usage personnel, je ne risque pas de créer des cas compliqués d’usage 😉
Cependant, une chose ne m’a pas paru intuitive. Lorsqu’on créé un dépôt tout neuf sans repartir d’aucun code existant, la commande push pour remonter mes commits ne semblait pas fonctionner correctement. En effet, avec les commandes ci-dessous :

```bash
git clone http://127.0.0.1:8080/gitblit/git/maintest.git
cd maintest
echo "TEST" > test.txt
git add .
git commit -a -m "Commit de test"
git push
```

Cette dernière commande n’a jamais rien affiché de plus qu’un magnifique :

```log
Everything up-to-date
```

Et rien n’est pas apparu dans mon dépôt.

Après quelques essais, ce que je comprends, c’est lors de la création du dépôt par Gitblit, aucune branche master n’existe (ou du moins, elle n’est pas correctement associée à l’origin du dépôt). Pour remettre les choses en place, la simplissime commande ci-dessous suffit à remettre les choses en place :

```bash	
git push origin master
```

Après ça, la branche master locale est bien reliée à la branche master sur origin (donc sur notre serveur Gitblit).

