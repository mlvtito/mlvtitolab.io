+++ 
date = "2013-03-10"
title = "Ajouter un JDK dans Ubuntu"
tags = []
categories = []
+++

En suivant le fil de mes articles, vous devriez savoir que dispose d’un serveur dédié sous Ubuntu. Sur ce serveur , j’héberge un serveur Glassfish. Jusqu’à maintenant, je me suis obstiné à faire tourner ce conteneur JEE avec l’OpenJDK qui se trouve par défaut dans les dépôts officiel de mon système d’exploitation.

Aujourd’hui, je n’en peux plus, après de multiples crash système de la JVM (tous les 3 jours environs). Je ne possède pas une analyse fine de la cause mais avant de me lancer dans une analyse des fichiers « core » générés, je vais simplement tenter d’utiliser le JDK officiel d’Oracle. Même si la base de ce dernier est le premier, ce dernier peut contenir un patch pour ma situation…qui sait !

Télécharger le JDK
------------------

Je ne pensais pas que ce serait une difficulté mais ce n’est pas aussi évident de télécharger le JDK depuis le site d’Oracle directement sur le serveur avec la commande  » wget ». En effet, avant de pouvoir télécharger tout fichier, il faut passer par la case d’acceptation de la license Oracle. La parade à cet obstacle passe par l’utilisation d’un analyseur de de requête HTTP (comme Firebug sous Firefox).

Dans le navigateur, avant de lancer effectivement le téléchargement de l’archive, le serveur d’Oracle lance une série de redirection :

![](../../assets/images/oracle-jdk-screenshot.png)

La dernière URL est de la forme « wget http://download.oracle.com/otn-pub/java/jdk/7u17-b02/jdk-7u17-linux-x64.tar.gz?AuthParam=1362942876_4bc4e1d256685c05747a32db3ca2d8ac ». C’est cette URL qu’il faut reprendre avec « wget ».

Installer le JDK
----------------

En soit, l’installation n’est pas compliqué, il suffit de décompresser l’archive téléchargée dans le répertoire de son choix (au hasard « /usr/lib/jvm »). Mais pour finir l’installation proprement, il est important d’ajouter le nouveau JDK dans la configuration des alternatives pour l’exécutable « /usr/bin/java ».

Sous Linux, la commande « alternatives » permet de maintenir des versions différentes pour des liens symboliques et de changer rapidement la cible du lient. L’ajout d’un JDK se fait donc simplement au travers de la commande :

```bash
update-alternatives --install /usr/bin/java java \
                /usr/lib/jvm/jdk1.7.0_17/bin/java 2000
```

Pour vérifier la bonne prise en compte, rien de plus simple, la commande ci-dessous vous donnera la liste des alternatives

```bash
update-alternatives --config java
```

Ces sites m’ont aidés à retrouver ces commandes et à comprendre de quoi s’agissait:

* http://linuxdrops.cAom/install-glassfish-with-jdk-7-on-centos-rhel-fedora-debian-ubuntu/
* http://linux.about.com/library/cmd/blcmdl8_alternatives.htm

