+++ 
date = "2012-02-20"
title = "Gitblit & Jenkins : l’authentification du risque"
tags = []
categories = []
+++

Tout récemment, je me suis amusé avec Gitblit et Jenkins. Je ne pensais vraiment rencontrer le moindre souci pour intégrer ces 2 outils (tellement ça me paraissait être l’état de l’art du développement). Oui mais en fait si, il y a bien quelques petits soucis. Pas grand chose me diront certain.

Gitblit est une application Web permettant de créer, organiser ses dépôts Git. Elle est assez jeune et très prometteuse je trouve car elle fonctionne out of the box (comme on dit). Nullement besoin d’installer un binaire git à côté de l’application ou un serveur HTTP pour que les développeurs accèdent aux dépôts. Toutes les couches de communications, la gestion des utilisateurs et la gestion des dépôts sont dans l’application Web.

Je ne présente plus Jenkins, l’outil d’intégration continue qui se charge d’extraire le contenu du gestionnaire de source, de builder, de tester, ….

Voici le problème (qui est une intersection de 2 problèmes) :

* Par choix, je veux des dépôts qu’on peut librement cloner mais je veux en restreindre les push. C’est génial, c’est tout à fait faisable dans Gitblit. Aïe ! Oui on peut le faire mais le clonage n’est pas vraiment libre car il demande un compte. 1er problème
* Jenkins a un plugin pour l’intégration d’un dépôt git. Super, je ne devrais pas avoir de problème particulier. Ah mais c’est pas possible, ils m’en veulent ! Impossible de saisir un login et un mot de passe pour accéder au dépôt. Qu’est-ce que c’est que ce plugin à moitié fini.

Résultat : mes dépôts sont en libre accès (ce qui n’est pas un problème pour moi car je suis le seul participant à mes projets) mais ça m’embêterait d’avantage dans une organisation plus conséquente en équipe.

