+++ 
date = "2013-02-02"
title = "Simple Git"
tags = []
categories = []
+++

Git est connu pour apporter une nouvelle façon de penser le travail collaboratif. Lui et et sa bande de copains que sont les gestionnaires de sources décentralisés ont définis de nouvelles organisations comme le « Integration-Manager Workflow » ou « Dictator and Lieutenant Workflow ». Ces workflows définissent des responsables pour les étapes de merge libérant ainsi le développeur de cette tache.

Oui mais (parce qu’il en faut toujours un) lorsqu’on passe d’un outil centralisé comme Subversion à Git, on ne change pas toute l’organisation. La création des branches conservent le même rythme et chaque développeur n’aura pas son dépôt publique de si tôt. C’est une habitude purement centralisée avec des branches par version qui sont souvent conservé. Cet article se veut être le guide de survie du Git centralisé en résumant les commandes élémentaires et nécessaire de connaître.

* git clone

C’est la première étape qui va vous permettre de cloner le dépôt à partir duquel vous souhaitez travailler.

```bash
git clone ssh://host:/mondepot
```

* git checkout

Cette commande va vous permettre de sélectionner la branche dans laquelle travailler. En effet, c’est l’ensemble du dépôt que vous avez cloné et pas uniquement une branche.

```bash
git checkout branch-new-feature
```

* git add

Vous venez de créer et modifier des fichiers dans votre espace de travail. La commande « add » va faire passer ces modifications dans l’espace de « staging ».

Il faut bien concevoir que lorsqu’on travail en local avec Git, on dispose de 3 espaces : l’espace de travail qui sont les répertoires et fichiers que nous manipulons directement, le dépôt qui est une copie du dépôt distant et qui enregistre les commits et il y a le « staging » (ou « index ») qui est une zone intermédiaire qui traque les fichiers qui vont faire parti du prochain commit

```bash
vi README
git add README
```

* git commit

La commande ressemble à celle des autres gestionnaires de sources. Et elle fait la même chose, elle enregistre les modifications associés à un message dans le dépôt mais uniquement dans votre copie locale du dépôt.

```bash
git commit
```

Pour que le commit prenne en compte les fichiers modifiés dans votre espace de travail mais non transité dans l’index, vous pouvez utiliser l’option « -a » (attention, ça ne prend pas en compte les nouveaux fichiers uniquement les modifiés).

* git push

Votre dépôt local a enregistré une série de commit et vous êtes satisfait de vos développements, il ne vous reste plus qu’à les envoyer sur le dépôt officiel avec cette simple commande :

```bash
git push
```

Git reprend la source de la commande c »clone » pour savoir où envoyer les commits enregistrés dans votre dépôt. Si d’autres commit ont été « pushés » avant vous sur le dépôt distant (à partir du moment où vous l’avez cloné), il faudra alors mettre votre dépôt à jour avant de pouvoir envoyer quoique ce soit.

* git pull

C’est la commande qui permet de mettre à jour le dépôt local à partir du dépôt distant. Dans le cas le plus simple (vous n’avez pas de modification sur votre dépôt local), la commande rapatrie toutes les modifications du dépôt. Si des modifications locales il existe, deux modes de fonctionnement il existe :

. En invoquant la commande sans option, nous faisons alors un « pull » en mode « merge ». Lorsque les commits distants sont rapatriés sur votre dépôt local, un commit supplémentaire contenant le résultat du merge automatique est créé.
. En invoquant la commande avec l’option « –rebase », nous faisons alors un « pull » en mode « rebase ». Lorsque les commits distants sont rapatriés sur votre dépôt local, vos commits locaux sont déplacés au sommets des commits et comprennent le résultat du merge automatique

Dans les deux cas, si le merge automatique ne fonctionne pas, la commande « pull » signalera les fichiers en conflit. Comme avec Subversion les fichiers en question seront marqués avec des extraits de la version local et distante et ce sera à vous de résoudre le conflit. Une fois un fichier résolu, il suffit de l’ajouter dans l’index avec la commande « add ».

Une fois tous les conflits résolus, s’il s’agissait d’un « pull » en mode « merge » et il ne reste plus qu’à commiter le merge avec la commande « commit ». Sinon, c’était un « pull » en mode « rebase » et il faut terminer le rebase :

```bash
git rebase --continue
```

Dans le cas d’une utilisation en mode centralisé, je pense qu’il est préférable d’utiliser des « pull » en mode « rebase ». En effet, sinon le dépôt laisse apparaitre des commits de merge qui n’ont pas vraiment de sens puisqu’aucune branche n’a réellement été mergé (même si dans les faits votre dépôt local est considéré comme une branche par Git).

Besoin de quelques références sur Git :

* http://git-scm.com/book
* http://blog.octo.com/git-dans-la-pratique-22/

