+++ 
date = "2013-08-14"
title = "Maven et la Javadoc"
tags = []
categories = []
+++

Voici une commande Maven qui dépanne bien pour compléter la Javadoc intégré à mon IDE préféré (en l’occurrence Netbeans).

Souvent, sur un projet Maven, on ajoute des dépendances sur tout un tas de librairies et, à chaque fois, on implore les dieux pour que la Javadoc vienne avec la librairie et soit directement intégré à l’IDE. Ça nous éviterait de bookmarker toutes les Javadoc en ligne :

![](../../assets/images/netbeans-javadoc-screenshot.png)


Bon, ça va pas venir automatiquement mais il suffit de lancer la commande Maven ci-dessous pour télécharger toutes ces Javadoc :

```bash
mvn dependency:resolve -Dclassifier=javadoc
```

A noter, qu’il est possible de télécharger les sources également (si vous aimez passer au débugger tout ce qui vous passe sous la main) avec la commande :

```bash
mvn dependency:resolve -Dclassifier=sources
```

Toutes les archives téléchargées se trouvent alors dans le repository local de Maven.
