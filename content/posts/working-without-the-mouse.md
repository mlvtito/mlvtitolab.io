+++ 
date = "2019-01-18"
title = "Travailler sans la souris"
tags = []
categories = []
+++

Régulièrement, je listerai les raccourcis clavier que j'utilise dans mon quotidien pour essayer de me passer la souris. Je rassemblerai ici les différents posts de ce type mais, que les choses soient bien claires, il ne s'agit pas de listes exhaustives de raccourcis. Je me concentre sur ceux que j'utilise et que je ne souhaite pas oublier. Bref, ce sont mes antisèches que je partage avec vous.

Pourquoi se passer de la souris me direz-vous ? Et bien j'ai 2 raisons (allez 2 raisons et demi).

## Faire du bien à son corps

La plus importante: si vous voulez faire carrière en tant que développeur vous allez passer de nombreuses heures sur un ordinateur. Si vous ne prenez pas soin de vos mains et de votre posture à votre bureau, dans un avenir lointain votre corps vous fera un signe et ce ne sera pas pour vous dire merci. Travaillez en minimisant les allers-retour clavier-souris, c'est imposer moins d'effort à votre épaule et vos poignets (surtout si vous avez choisi un clavier confortable).


## Etre plus efficace

L'autre raison qui a son importance: vous gagnez en productivité. Si d'un coup de raccourci vous savez activer la sélection en colonne dans votre éditeur de texte et sélectionner jusqu'à une certaine ligne. Ce sera 100 fois plus efficace que de trouver le bouton pour changer le type de sélection et scroller jusqu'à la bonne ligne.

## Les demi-raisons

Apprendre à travailler sans souris et sans regarder son clavier, ça en jette. Pourtant, je pense que je ne suis pas le plus aguerri dans cet exercice. Mais ça fait son effet quand on finit de taper ce qu'on a à écrire en regardant droit dans les yeux la personne qui vient vous poser une question.


* [Les raccourcis de mon gestionnaire de fichiers Nautilus]({{< relref "shortcuts-nautilus.md" >}})


