+++ 
date = "2012-09-03"
title = "Postgres et ses séquences"
tags = []
categories = []
+++

C’est un petit truc à savoir mais ça peut rendre chèvre si on ne le sait pas.

Lorsque vous faites un dump d’une base PostgreSQL avce par exemple PHPPGAdmin, les dernières lignes du fichier généré sont là pour mettre à jour les séquences de la base de données.

Oui mais voilà, de temps en temps, ces lignes sont de la forme :

```sql
SELECT pg_catalog.setval('seq_role', 100, true);
```

Et les autres fois, ça sera de la forme :

```sql
SELECT pg_catalog.setval('seq_role', 100, false);
```

Si vous modifiez le script à la main après l’avoir dumpé, faites attention, ce flag qui prend la valeur « true » ou « false » définit si la valeur passée en paramètre est la suivante qui sera fournie par la séuqence ou si c’est la précédente valeur.

```sql	
-- le prochain appel retournera 101
SELECT pg_catalog.setval('seq_role', 100, true); 
 
-- le prochain appel retournera 100
SELECT pg_catalog.setval('seq_role', 100, false);
```

Pensez-y quand vous aurez vos prochaines violations de contraintes.
