+++ 
date = "2016-10-14"
title = "Bootstrap d’un projet Aurelia"
tags = []
categories = []
+++

Je vais tacher de conserver ici les commandes que j’utilise pour démarrer un projet Aurelia en typescript.

On installe JSPM sur le projet :

```bash	
npm install jspm@0.17.0-beta.29 --save-dev
```

Ensuite, on intialise le projet:

```bash	
jspm init
Package.json file does not exist, create it? [Yes]: Yes
Init mode (Quick, Standard, Custom) [Quick]: Quick
Local package name (recommended, optional): app
package.json directories.baseURL: .
package.json configFiles folder [./]: ./
Use package.json configFiles.jspm:dev? [No]: No
SystemJS.config browser baseURL (optional): .
SystemJS.config Node local project path [src/]: src/
SystemJS.config local package main [app.js]: app.ts
SystemJS.config transpiler (Babel, Traceur, TypeScript, None) [typescript]: typescript
```

J’apporte ensuite quelques corrections dans le fichier « jspm.config.js ». Tout d’abord, dans le package « app », j’ajoute l’extension par défaut :

```
"defaultExtension": "ts",
```

Et j’ajoute un path :

```	
"*": "src/*"
```

Ensuite, j’installe mes dépendances à Aurelia :

```	
jspm install aurelia-bootstrapper -y
```

Pour effectuer des tests et valider la bonne initialisation, j’utilise superstatic :

```	
npm install superstatic -g
superstatic
```

