+++ 
date = "2022-11-14"
title = "Archives tar et comrpessions"
description = "Cet article résumé les compressions associables à la commande tar et redonne les options pour les activer"
tags = ["linux", "tar", "gzip", "bzip2", "xz"]
categories = ["Ops", "Linux"]
+++

Inévitablement, si vous manipulez des fichiers sur des systèmes Unix/Linux, vous allez voir passer des archives `.tar` ou `.tar.gz`. Je me souviens encore d'une époque ou j'achetais mon [Linux Magazine](https://www.linux-magazine.com/) chez le marchan de de journaux de ma ville, après avoir lu le contenu du magazine, je m'attaquais au contenu du CD qui était fourni avec. Et c'est là que j'ai découvert ces archives pour la première fois. Tout cela me paraissait bien mystérieux.

Maintenant que j'ai trahis mon age, j'en reviens au contenu de cet article. Depuis cette époque, les algorithmes de compression ont évolué et le fameux [gzip](http://www.gzip.org/) n'est plus le seul player sur nos environnements Linux. J'écris donc cet article qui va synthétiser les options à passer à la commande [tar](https://www.gnu.org/software/tar/) pour choisir la compression à appliquer. Je vais également référencer un autre article qui a pris le temps de faire des tests de performances. Les résultats de ces tests sont une aide pour choisir quelle compression dans quelle circonstance.

### Les options de commandes

Petit rappel, si vous voulez créer une archive `tar`simple sans compression, voici la commande:

```
tar cvf mon_archive.tar ./contenu_a_inclure_dans_larchive/
```

La création d'une archive `gzip` se fait via:
```
tar czvf mon_archive.tar.gz ./contenu_a_inclure_dans_larchive/
```

Pour une archive `bzip2`:
```
tar cjvf mon_archive.tar.bz2 ./contenu_a_inclure_dans_larchive/
```

Et enfin, si on veut appliquer la compression `xz`:
```
tar cJvf mon_archive.tar.xz ./contenu_a_inclure_dans_larchive/
```
### Quels compression choisir ?

Pour notre culture générale, passons en revue les compressions dont nous parlons. 

[gzip](http://www.gzip.org/) fait référence à une combinaison des algorithmes LZ77 et Huffman. La première version date de 1992.

[bzip2](https://sourceware.org/bzip2/) fait référence à l'algorithme `bzip2`. Au moins, sur ce coup là, le nom de l'outil est explicite. La première version date de 1996.

[xz](https://tukaani.org/xz/format.html) est en fait une extension de fichier générique conçu pour accueuillir tout type de compression. Ceci étant dit, l'algorithme de compression par défaut dans ce fichier le LZMA2. pour de vrai, pour le moment, on ne peut trouver que des compressions LZMA et LZMA2 dans les fichiers `xz`. Le support de LZMA2 semble exister depuis 2009.

Vous trouverez les détails sur les performances de chaque outils [ici](https://www.rootusers.com/gzip-vs-bzip2-vs-xz-performance-comparison/).

Personellement, ce que j'en retiens, c'est que :
* j'utilise `gzip` pour une compression rapide avant un transfert via SSH par exemple où je veux juste réduire la consommation de la bande passante.
* j'utilise `xz` dans les cas où le fichier compressé vivra plus longtemps car le fichier obtenu est bien plus petit. Par contre, on fera attention à s'assurer que les resources de la machine sont bien disponibles pour la compression (a.k.a. ne pas le faire sur une machine de production).

Finalement, `bzip2` qui se retrouve entre les 2 autres aura peu de cas d'utilisation pour moi.


