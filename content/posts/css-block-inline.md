+++ 
date = "2012-10-30"
title = "CSS : tous en bloque et en ligne"
tags = []
categories = []
+++

Voici le petit hack dont il faut se souvenir avoir une propriété « display: inline-block; » qui fonctionne sous Internet Explorer. Il faut l’ajouter les propriétés suivante :

```css
*display:inline; 
zoom:1; /* "inline-block" for IE6/7 */
```

Et pendant que j’y suis, la valeur « auto » sur la propriété « margin » ne fonctionne sous IE que si le parent a une propriété « text-align: center » de positionné.

Sources:

* http://robertnyman.com/2010/02/24/css-display-inline-block-why-it-rocks-and-why-it-sucks/
* http://forum.alsacreations.com/topic-4-40741-1-Probleme-IE8—margin–0-auto-.html

