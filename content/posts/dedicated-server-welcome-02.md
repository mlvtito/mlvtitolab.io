+++ 
date = "2011-12-01"
title = "Bien recevoir son serveur dédié (suite)"
tags = []
categories = []
+++

Où est-ce que j’en étais ? Ha oui, on a créé l’utilisateur qui va nous servir d’administration avec les droit sudo qui vont bien. C’est bien beau mais on peut encore protéger notre utilisateur root un peu plus. Et pour cela, on peut simplement empêcher les connexions à distance avec le compte root. Editer le fichier /etc/ssh/sshd_config et remplacer avec la valeur ci-dessous :

```apache
PermitRootLogin No
```

Ce qui est marrant, c’est que sans n’avoir encore rien fait de concret avec ce serveur, on peut voir les attaques de force brut pour se connecter avec des utilisateurs courant. Voici un extrait de mon /var/log/auth.log :

```log
Nov 30 07:40:39 XXXXXXXX sshd[27464]: Failed password for root from 91.121.166.49 port 51087 ssh2
Nov 30 07:40:39 XXXXXXXX sshd[27466]: error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Nov 30 07:40:39 XXXXXXXX sshd[27466]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=ks360976.kimsufi.com  user=root
Nov 30 07:40:42 XXXXXXXX sshd[27466]: Failed password for root from 91.121.166.49 port 51305 ssh2
Nov 30 07:40:42 XXXXXXXX sshd[27468]: error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Nov 30 07:40:42 XXXXXXXX sshd[27468]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=ks360976.kimsufi.com  user=root
Nov 30 07:40:44 XXXXXXXX sshd[27468]: Failed password for root from 91.121.166.49 port 51640 ssh2
Nov 30 07:40:44 XXXXXXXX sshd[27470]: error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Nov 30 07:40:44 XXXXXXXX sshd[27470]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=ks360976.kimsufi.com  user=root
Nov 30 07:40:46 XXXXXXXX sshd[27470]: Failed password for root from 91.121.166.49 port 51833 ssh2
Nov 30 07:40:46 XXXXXXXX sshd[27472]: error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key
Nov 30 07:40:46 XXXXXXXX sshd[27472]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=ks360976.kimsufi.com  user=root
```

Ici, ce sont plusieurs tentative de connexion avec l’utilisateur root (d’où ma soudaine prise de conscience d’interdire tout accès root à distance). Des logs de ce genre, on y voit des utilisateur oracle, irc, nagios, postgres ou encore trac.

De ces observation, je vais en retirer 3 conclusions :

* tout mot de passe de ce serveur sera complexe et généré par un outil du type keepass
* lorsque j’installerai ma suite logiciel, je choisirai soigneusement et personnellement des noms d’utilisateur totalement atypique
* avoir un serveur, aussi petit soit-il, ça rend paranoïaque ; le prochain article de ce serveur dédié portera très certainement sur un firewall


