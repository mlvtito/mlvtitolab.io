+++ 
date = "2019-06-25"
title = "Gérer plusieurs compte Gitlab"
tags = []
categories = []
+++

Je me note ici une petite astuce pour gérer de multiples compte utilisateur sur Gitlab tout en conservant la possibilité de se connecter en utilisant les certificats SSH.

Peut-être suis-je le seul mais j'aime cloisonner mes activités. Ne pas mélanger mes activités liées à ma société, celle lié à ma participation open source et celle liée à mon client.

C'est pourquoi je me retrouve avec plusieurs comptes sur la plate-forme Gitlab.

Lorsqu'on accède à un repository en SSH, Gitlab va nous donner une adresse du genre :

```
git@gitlab.com:mon-login-perso/mon-projet-perso.git
```

Les questions qui nous viennent à l'esprit sont :

* comment je décide le login à utiliser pour se connecter ?
* comment je précise la clef SSH à utiliser ?

C'est la versatilité de SSH qui va nous aider. En effet, dans le fichier de configuration de SSH sur notre poste client, il est possible de définir un hostname totalement inventé qui va rediriger vers un véritable serveur tout en précisant le login à utiliser et le fichier de clef SSH privé.

Voici un exemple :

```
Host gitlab.mon-fake-domain-perso
  Hostname gitlab.com
  User mon-login-perso
  IdentityFile ~/.ssh/id_gitlab_perso
```

Il suffira ensuite d'utiliser ce faux nom de domaine pour les actions avec git :

```
git clone git@gitlab.mon-fake-domain-perso:mon-login-perso/mon-projet-perso.git
```


