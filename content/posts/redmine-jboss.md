+++ 
date = "2012-05-03"
title = "Redmine, Rails, Warbler, JBoss : la magie de JRuby"
tags = []
categories = []
+++

J’ai toujours regreté de ne pas trouver un outil de bug tracking Open Source et sympa dans l’écosystème Java. Certes, il y a bien Jira qui offre une licence au projet Open Source (je trouve que c’est d’ailleurs une très bonne démarche de leur part qui permet de conserver leur business model tout en soutenant la communauté). Mais c’est bien loin de mon business model personnel composé à 80% de vaporware.

De plus, j’ai toujours été attiré, intrigué, piqué au vif par Redmine. Les quelques heures de prise en main que j’ai pu en faire m’ont fait très bonne impression. Il n’a pas l’air aussi personnalisable que Jira mais l’interface respire la simplicité et l’évidence, je ne me suis jamais posé de question sur ce que je faisais.

Je désespérais donc de trouver un outil équivalent à déployer sous mon JBoss ou mon Tomcat et j’en venais à la conclusion, mainte fois partagé, que finalement on est jamais mieux servi que par soi-même. Et là, je suis tombé sur cet article et ma vie a changée :

http://www.javacodegeeks.com/2012/02/redmine-installation-getting-started.html

Et toute cette magie repose sur Warbler un gem à installer qui permet d’encapsuler les applications Rails dans un package war. Le tout utilisant JRuby pour l’exécution. Et je garantie que les explications données dans l’article ci-dessus fonctionne à merveille pour un Redmine 1.3.1 déployé sur un JBoss 7.1.0. Et pour avoir un peu plus de détail sur le fonctionnement de Warbler :

http://jrubyist.wordpress.com/2009/10/15/using-jruby-warbler-rake-to-deploy-rails-apps-to-jboss/


