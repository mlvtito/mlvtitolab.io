+++ 
date = "2011-10-22"
title = "Bien recevoir son serveur dédié"
tags = []
categories = []
+++

Cette nuit, j’ai reçu un petit serveur dédié provisionné par Kimsufi/OVH avec une pré-installation Ubuntu. Il devrait me servir à mettre en place ma propre plate-forme d’intégration continue en Java et autre Grails…mais, je n’y suis pas encore ; j’y reviendrai sûrement.

Si vous commandez un de ces serveurs, sachez que la disponibilité affiché d’1 heure ne correspond à rien car, vous passerez par des étapes de validation de vos moyens de paiements. Pour ma part, ça a prit environ 7 heures mais si vous n’avez pas de chance, vous aurez des papiers à renvoyer pour validation.

Le serveur reçu est fourni avec un compte root et si il y a bien une habitude à ne pas prendre, c’est celle de ne pas utiliser quotidiennement ce compte mais d’en passer par un autre. Donc, on créé son compte utilisateur tout de suite :

```bash
useradd -m -d /home/moncompte moncompte
passwd moncompte
```

Maintenant, pour ne plus jamais avoir à utiliser ce compte (comme dans les installations par défaut d’Ubuntu), il faut configurer ce nouveau compte pour l’autoriser à faire des sudo. Rien de plus simple, il suffit d’appartenir au groupe sudo :

```bash
adduser moncompte sudo
```

Reconnectez vous avec votre nouveau compte fraîchement créé et mettez à jour votre distrib :

```bash
sudo apt-get update
sudo apt-get upgrade
```

Voici le minimum vital que j’ai appliqué dès réception. Et je vous conseille d’en faire autant et de ne jamais utiliser de telnet et de ftp, vous pouvez tous faire avec SSH.

