+++ 
date = "2019-01-18"
title = "Nautilus shortcuts and customization"
tags = []
categories = []
+++

Voici mon memento de raccourcis pour Nautilus que j'utilise quotidiennement.

A ajouter à la liste ci-dessous, j'utilise aussi de la touche d'affichage du menu contextuel (l'équivalent du click droit avec la souris). J'en abuse en particulier lorsque je veux créer un nouveau fichier.

## Les fenêtres

* <kbd>Ctrl</kbd> + <kbd>N</kbd> : Ouvrir une nouvelle fenêtre

## Les onglets

* <kbd>Ctrl</kbd> + <kbd>T</kbd> : Ouvrir un nouvel onglet
* <kbd>Ctrl</kbd> + <kbd>Return</kbd> : Ouvrir le répertoire sélectionné dans un nouvel onglet
* <kbd>Ctrl</kbd> + <kbd>W</kbd> : Fermer l'onglet courant ou la fenêtre  s'il n'y a plus d'onglet
* <kbd>Ctrl</kbd> + <kbd>PgDown</kbd> : Aller à l'onglet suivant
* <kbd>Ctrl</kbd> + <kbd>PgUp</kbd> : Aller à l'onglet précédent

## Navigation

* <kbd>Alt</kbd> + <kbd>Home</kbd> : Aller sur son dossier personnel
* <kbd>Ctrl</kbd> + <kbd>Up</kbd> : Aller sur le répertoire parent
* <kbd>Ctrl</kbd> + <kbd>Left</kbd> : Aller sur le répertoire précédent
* <kbd>Ctrl</kbd> + <kbd>L</kbd> : Aller à un emplacement spécifique. Ici il s'agit d'écrire soit même le chemin mais la complétion automatique est proposée

## Les fichiers

* <kbd>Return</kbd> : Ouvrir le répertoire ou le fichier sélectionné
* <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>N</kbd> : Créer un répertoire
* <kbd>Ctrl</kbd> + <kbd>H</kbd> : Montrer/cacher les fichiers cachés
* <kbd>Alt</kbd> + <kbd>Return</kbd> : Ouvrir la fenêtre de propriétés du fichier sélectionné ou du répertoire courant
* <kbd>F2</kbd> : Renommer
* <kbd>Ctrl</kbd> + <kbd>F</kbd> : Rechercher

## Les sélections

* <kbd>Ctrl</kbd> + <kbd>S</kbd> : Sélectionner les fichiers selon une expression régulière
* <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd> : Inverser la sélection
* <kbd>Ctrl</kbd> + <kbd>Space</kbd> : (Dé)selectionner le fichier courant. Si plusieurs fichiers sont sélectionné, il faudra d'abord n'en sélectionné plus qu'un avec les flèches par exemple puis désélectionner ce dernier.









