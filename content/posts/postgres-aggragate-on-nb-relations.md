+++ 
date = "2018-12-18"
title = "Postgres : statistiques sur le compte d'une relation"
tags = []
categories = []
+++

Imaginez un instant que vous avez un modèle de données tout simple constitué d'une entité "Blog" et d'une autre entité "Article". Bien entendu, il y a une relation 1-N entre les deux, un "Blog" peut avoir un nombre indéterminé d' "Article".

Mettre un schéma

Voici la requête qui vous permettra d'avoir quelques statistiques sur cette relation : 

```sql
select percentile_disc(0.9) WITHIN GROUP (ORDER BY nb_articles), avg(nb_articles), max(nb_articles)
from (
    select b.name as blog_name, count(a.id) as nb_articles
    from blog b
    inner join article a on a.blog_id = b.id
    where b.statut = 'ONLINE'
    group by b.name
) as foo;
```


