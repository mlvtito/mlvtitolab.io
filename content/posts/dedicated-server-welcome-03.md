+++ 
date = "2011-12-13"
title = "Bien recevoir son serveur dédié (suite part 2)"
tags = []
categories = []
+++



J’ai dans mon panier 2 solutions à mes problèmes d’attaque de force brute sur ma connexion SSH :

* le démon denyhosts
* et, plus simplement, changer le port par défaut d’écoute de SSH

denyhosts
---------

denyhosts, c’est un processus daemon qui scrute votre fichier de log où se trouve toutes les traces de tentatives de connexions. Toutes les hosts qui génèrent trop d’erreur de connexion seront blacklisté.

L’installation prend 2 secondes sous Ubuntu :

```bash
sudo apt-get install denyhosts
```

Et voilà, c’est fait et c’est fonctionnel. Vous trouverez les fichiers listant les tentatives suspicieuses de connexion dans :

```bash	
/var/lib/denyhosts
```

Il ne restera plus qu’à compléter le fichier de configuration avec un serveur d’envoi des mails pour recevoir une alerte à chaque fois qu’une adresse est ostracisée.

Changer de port
---------------

Par défaut, SSH écoute sur le port 22. Il est facilement modifiable dans le fichier :

```bash	
/etc/ssh/sshd_config
```

Puis par un simple arrêt / relance du démon SSH :

```bash	
/etc/init.d/sshd restart
```

Vous devriez voir le nombre de tentative se réduire drastiquement.

Toutefois, il faut être vigilant sur cette dernière astuce (et sur celle qui consiste à empêcher l’utilisateur root de se connecter). Je ne sais pas comment ça se passe pour les autres hébergeur mais chez OVH, lorsqu’ils livrent un serveur, ils installent une clef pour pouvoir se connecter en root lorsqu’une intervention est nécessaire. Interdire l’utilisateur root ou changer le numéro de port rendra toute intervention de l’hébergeur impossible. Renseignez-vous bien auprès de ce dernier ou, au moins, ayez conscience que vous serez à bord et que si vous n’avez plus accès à votre machine, il n’y aura plus d’autre choix que de la réinstaller (d’où l’importance d’avoir un plan de sauvegarde bien roder 🙂 ).



