+++ 
date = "2011-11-16"
title = "La puissance du côté négatif de la force"
tags = []
categories = []
+++

Le côté négatif, c’est tout simplement le caractère ‘–‘ et la force, chez nous aujourd’hui, est la shell Unix. Ce fameux caractère dash est un fabuleux outil à plusieurs facettes. Connaître ses possibilité est au moins aussi (si ce n’est plus) essentiel que de connaître quelques raccourcis claviers (comprendre que ça vous fera gagner du temps. Après si vous êtes pas pressés dans la vie, vous pouvez passer votre chemin).


== Le caractère dash comme représentation du dernier répertoire

C’est un classique, taper la commande suivante pour revenir dans le répertoire précédent

```bash
cd -
```

Par exemple :

```bash
[user@host:/etc]$ cd /var/log
[user@host:/var/log] cd -
[user@host:/etc] cd -
[user@host:/var/log] cd -
[user@host:/etc]
```

On ne remonte pas l’historique des répertoires mais on est bien content quand on se trouve dans la situation où on lance une application, on regarde les logs, on change une configuration, on relance, on vérifie les logs, on change une configuration, on relance, on vérifie les logs, … Cette boucle vertueuse se révèle bien plus rapide, avec un ‘cd –‘ et une (ou 2) commande(s) dans le presse papier.

== Le caractère dash comme redirection d’entrée/sortie

C’est dans ce chapitre que la force nécessite du contrôle. Bien caché dans une commande le caractère ‘–‘ représente une redirection d’un fichier vers la sortie standard ou inversement  de l’entrée standard vers un fichier. Les exemples suivant devraient illustrer mon propos. Imaginons que je sois en pleine rédaction de scripts pour archiver des fichiers de log ou manipuler ces mêmes archives. Basiquement pour archiver, j’utiliserai les commandes suivantes :

```bash
tar cf archives_20111116.tar *.log
bzip2 archives_20111116.tar
```

un inconvénient qui (devrait) saute(r) au yeux est l’utilisation de ce fichier archives_20111116.tar à la durée de vie très limitée et au moins de taille égale aux fichiers de logs que je veux archiver. Heureusement la caractère ‘-‘ est là pour nous sauver, on recommence et on remplace par :

```bash
tar cf - *.log | bzip2 > archives_20111116.tar.bz
```

Ici, la commande tar écrit sur la sortie standard au lieu d’écrire dans un fichier, il n’y a donc plus de fichier temporaire, tout transite en mémoire au fur et à mesure que la compression avance.

C’est pas tout, mais j’aimerai bien les décompresser moi ces archives mais sans les altérer et sans utiliser de fichier temporaire (comme à l’archivage en somme). Et bien, la force reste avec vous jusqu’au bout :

```bash
bzip2 -d archives_20111116.tar.bz | tar xf -
```

La commande tar lit l’entrée standard au lieu de lire le contenu d’un fichier. Je ne pense pas que vous vous rendiez compte de la magie qui se cache derrière ces manipulations ; l’exemple ici est plutôt simple et basique mais en gardant constamment cette fonctionnalité à l’esprit, on obtient des commandes puissantes en une seule ligne. Mon dernier exemple en date : une comparaison de fichier local et distant :

```bash
ssh user@host "cat /etc/hosts" | diff -wb /etc/hosts -
```

== Le caractère dash comme délimiteur d’option

Sur une commande, tous les mots qui commence par ‘-‘ sont des options. A priori, je vous apprends rien. Mais alors comment se dépêtrer des noms de fichier qui commencent par ce même caractère ? Ou comment s’assurer que l’utilisateur n’ajoutera une option insidieusement au sein d’un de vos shell ? La réponse : le double dash ‘–‘. Le double dash indique la fin des options. Ceci signifie que tout ce qui se suit sur la commande ne sera pas considéré comme une option. Un exemple valant mieux qu’un long discours, voici comment faire pour supprimer un fichier nommé ‘-filename‘ :

```bash
rm -f -- -filename
```

Malheureusement, tout les shell n’implémente pas ce séparateur d’options et, à ma connaissance, seul le Bash le propose.

Allez, maintenant, il est temps de renouveler ce classique shell d’archivage et de sans cesse l’améliorer (comme ce qu’on devrait faire dans tout ce qu’on entreprend).


