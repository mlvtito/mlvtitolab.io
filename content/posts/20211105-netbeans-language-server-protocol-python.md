+++ 
date = "2021-11-05"
title = "NetBeans - Ajouter Python grace au Language Server Protocol"
description = "NetBeans offre la possibilité de configurer n'importe quel language de développement via un server LSP"
tags = ["netbeans", "lsp", "language server protocol","python"]
categories = ["Devs"]
+++

Python est le dernier language à la mode. Il approche de la tête de tous les classements de language de programmation. La multitude de librairie d'intelligence artificielle le rend incontournable.

Vous comprendrez ma frustration de ne pas voir de plugins le supportant dans mon IDE préféré, c'est à dire NetBeans.

Heureusement, les dévelopeurs de NetBeans ont eu la bonne idée d'implémenter l'interfaçage avec le Language Server Protocol de manière assez générique.

Pour rappel, LSP est un protocol qui définit la manière dont un processus serveur peut offrir les fonctionalités d'édition des IDE (formatage, autocomplétion, ...). Une fois qu'un IDE est capable d'interroger le protocol LSP, il n'y a plus qu'à trouver le serveur correspondant au langage souhaité. Et des serveurs, ils commencent à y en avoir [quelques uns](https://microsoft.github.io/language-server-protocol/implementors/servers/).

Voici comment, j'ai procédé pour ajouter Python:

* Installer un serveur LSP pour Python (celui que j'ai utilisé vient de [ce site](https://github.com/palantir/python-language-server))

```bash
pip install 'python-lsp-server[all]'
```

* Se rendre dans le menu "Tools > Options"
* Récupérer un fichier de grammaire TextMate pour ce même langage. [Voici celui que j'ai utilisé](https://raw.githubusercontent.com/microsoft/vscode-textmate/main/test-cases/first-mate/fixtures/python.json)
* Se placer sur l'onglet "Editor" et le sous-onglet "Language Servers
* Enfin ajouter le langage avec les valeurs ci-dessous (le chemin du serveur peut être à adapter)

| Paramètre       | Valeur                         | Description
|-----------------|--------------------------------|--------------------------------------------------------
| Extensions      | py                             | L'extension des fichiers pris en charge par ce serveur
| Syntax Grammar  | /home/mlvtito/python.json      | Le chemin vers la grammaire TextMate
| Language Server | /home/mlvtito/.local/bin/pylsp | Le chemin vers l'exécutble du serveur
| Name            | Python                         | Un nom pour le nouveau langage
| Icon            | /home/mlvtito/python.json      | Une icone qui mettra en avant les fichiers de ce type (attention, l'icone doit faire du 16x16 pixels)

Et c'est tout. Voilà un NetBeans pour Python totalement opérationel.

![](../../assets/images/netbeans-python-completion.png)


