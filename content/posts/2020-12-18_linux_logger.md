+++ 
date = "2020-12-18"
title = "Le logger Linux - Utile pour vos jobs Cron"
tags = ["Linux", "Ubuntu", "logger","syslog","cron"]
categories = ["Ops"]
+++

Je me souviens d'une époque où les sorties standards des jobs planifiés dans la crontab aterrissaient dans la boite mail de l'utilisateur root du serveur. 

Bien sûr, le plus souvent, nous découvrions cette "fonctionnalité" lorsqu'une demande du genre "es-tu sûr que ton batch a bien tourner il y a 2 mois dans la nuit du samedi au dimanche entre 23h53 et 2h32 ?" arrivait dans nos mains. Et alors là, bonne chance pour tout dépiler.

Dans un contexte d'entreprise, j'ai bon espoir que ces logs soient collectées de manière industriel par un ordonnanceur global et aggégés dans un outil du genre Graylog.

Et puis, un jour, nous entrons dans cette entreprise et nous voyons bien que, bien trop souvent, la structure tient avec quelques bouts de ficelles et de scotch. Je m'éloigne de mon sujet mais la conclusion est qu'il faut se rabattre sur des outils plus fondamentaux.

Le plus fondamental des logger sous Linux est la syslog. Nous pouvons y ajouter notre sortie standard aussi simplement que ça:

```bash
* * * * * /opt/scripts/my-batch.sh 2>&1 > logger -t MY-IDENTIFIER
```

Vous pourrez voir le résultat directement dans le fichier /var/log/syslog. Elle ressemblera à ceci :

```bash
Dec 18 11:28:19 h1 MY-IDENTIFIER: #################### Processing
```

Maintenant que c'est dans la syslog, vous pouvez facilement exploiter son contenu avec la commande journalctl

```bash
prompt> journalctl -t MY-IDENTIFIER
-- Logs begin at Fri 2020-10-09 08:59:22 CEST, end at Fri 2020-12-18 11:33:22 CET. --
déc. 18 11:28:19 h1 MY-IDENTIFIER[16368]: #################### Processing

```

Voyons de plus prêt comment distinguer la sortie standard et la sortie d'erreur à l'aide du logger. L'option magique à utiliser est "-p" mais la ligne de cron devient plus compliquée :

```bash
* * * * * /opt/scripts/my-batch.sh 2> >(logger -t MY-IDENTIFIER -p local0.error) | logger -t MY-IDENTIFIER
```
Et bienvenue dans le monde de la couleur:

![](../../assets/images/ubuntu-logger-with-errors.png)

Utiliser les outils fondamentaux ouvre la porte à tout un éventail de possibilité comme:
* [centraliser les logs à distance sur un serveur centralisé](https://www.digitalocean.com/community/tutorials/how-to-centralize-logs-with-journald-on-ubuntu-20-04)
* [faire avaler toutes ces logs dans une interface avec recherche](https://docs.graylog.org/en/4.0/pages/sending/journald.html)


