+++ 
date = "2019-03-01"
title = "Optimisation de son poste de développement"
tags = []
categories = []
+++

Nous, développeurs, sommes de grand consommateurs de ressources sur nos postes de développement. Nos IDE, serveurs applicatifs, procédures de test qui se rechargent ou se relancent au moindre changement monopolisent un grand nombre de descripteurs de fichier par exemple (et tout ça pour ne pas faire F5 sur son navigateur).

Bien souvent, le paramétrage par défaut n'est pas suffisant. Je vous donne ici le minimum que j'applique à tout nouveaux postes sous Linux.


### /etc/sysctl.conf

Après avoir ajouté les quelques lignes ci-dessous, vous pourrez utiliser la commande ``sysctl -p`` pour en profiter sans attendre le prochain redémarrage.

* Optimisation de inotify

    inotify est la partie du noyau Linux qui fournit les notifications concernant le système de fichier.

    Il est crucial pour nos IDE qui observent les changements sur les fichier grace à lui.
  
   + ``fs.inotify.max_user_watches=999999``

        C'est tout simplement le nombre de fichiers qui peuvent monitorés par inotify.

   + ``fs.inotify.max_user_instances=999999``

        C'est le nombre d'instance inotify possible par processus.

   + ``fs.inotify.max_queued_events=999999``

        C'est le nombre d'évènement qui peuvent attendre en file pour une instance inotify.


* ``vm.max_map_count=262144``

C'est le nombre de memory maps allouables par processus. Mais soyons clair entre nous, ça n'a rien à voir avec la quantité de mémoire qui est concrètement allouée dans chaque map. Typiquement, ce paramètre est important pour le fonctionement des moteurs d'indexation : https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

### Optimiser le nombre de descripteurs de fichiers

Enfin pour pouvoir profiter de toute cette capacité à surveiller nos fichiers et indéxer nos contenus, il faut pouvoir ouvrir suffisemment de descripteurs.

#### /etc/systemd/system.conf & /etc/systemd/user.conf

* ``DefaultLimitNOFILE=999999``

    Pour les OS sous systemd, chaque processus est un fils d'un processus systemd qui garde un contrôle sur ses enfants. La valeur est prise en compte au démarrage d'un processus, il vous faudra donc relancer votre logiciel.

#### /etc/security/limits.d

Enfin, nous pouvons régler les garde-fous de Linux. Dans ce répertoire, j'ai ajouté un fichier ``90-locallimits.conf`` avec le contenu suivant :

```
* soft     nproc          150000
* hard     nproc          150000
* soft     nofile         150000
* hard     nofile         150000
root soft     nproc          150000
root hard     nproc          150000
root soft     nofile         150000
root hard     nofile         150000

```

Je vous invite à faire ces changements le plus tôt possible pour ne pas vous prendre la tête. Les messages d'erreur sont parfois incompréhensible (je pense à inotify qui m'a fait croire que mon FS était plein).



