+++ 
date = "2012-03-08"
title = "Back to basics of JSTL"
tags = []
categories = []
+++

En ce moment, je travaille sur une mini application permettant de collecter des données de performances de mon application mesurées pendant l’exécution de mes tests Selenium. Pour effectuer les mesures, je me base sur dynaTrace Ajax Edition qui installe un plugin sur les navigateurs contrôlés par Selenium et envoi les données à mon application qui se chargera de stocker les données et offre un écran pour les restituer.

J’ai voulu la faire simple, en utilisant un projet de Servlet et JSP tout ce qu’il y a de plus basique. Pour que ma page JSP soit belle, j’ai aussi voulu utiliser les JSTL. Et je dois bien l’avouer, cela a nécessité un rafraîchissement sur les versions de JEE et les compatibilités avec les versions de JSTL.

Qu’est ce que JSTL ?
--------------------

JSTL est l’acronyme pour Java Standard Tag library. Elle offre 2 choses :

* comme son nom le laisse entendre, une bibliothèque de taglib (pour manipuler les variables, contrôler son exécution par des tests et des boucles, …)
* les Expression Language qui permettent d’écrire des mini scripts Java dans une syntaxe ${ // mon code Java }

Les versions de JSTL
--------------------

Bien entendu, JSTL est une spécification qui a évolué au cours du temps. Voici, ici les correspondances de version JEE :

Servlet | JSP | JSTL | JEE
------- | --- | ---- | ---
2.5 | 2.1 | 1.2 | 5
2.4 | 2.0 | 1.1 | 1.4
2.3 | 1.2 | 1.0 | 1.2

Avec la version 1.2 de JSTL les Expression Language ne pouvaient être utilisées qu’au travers de la balise c:out. Pour les versions suivantes, les EL peuvent être utilisées n’importe où dans la page JSP.

Il faut donc être vigilant. Votre projet doit être dans une bonne version. Si vous utilisez Maven, votre fichier pom doit contenir les bonnes références :

```xml
<dependency>
     <groupId>javax.servlet</groupId>
     <artifactId>servlet-api</artifactId>
     <version>2.5</version>
     <scope>provided</scope>
</dependency>
```

Votre web.xml doit avoir la bonne entête. Pour une version 2.5 de Servlet :

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app
    xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
    id="WebApp_ID" version="2.5">
...
</web-app>
```

Il faut également être vigilant sur la manière dont on référence les taglib dans les JSP. En version 1.0, il faut utiliser la syntaxe suivante :

```jsp
<%@taglib uri="http://java.sun.com/jsp/core" prefix="c" %>
```

Pour les versions plus récentes, voici la ligne :

```jsp
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
```

Implémentation de JSTL
----------------------

JSTL est une spécification. Pour fonctionner, il faut disposer d’une implémentation. Heureusement la plupart des fournisseurs de serveurs d’application (comme Glassfish ou JBoss) fournisse une implémentation.

Mais ce n’est pas le cas de Tomcat. Et pour remédier à ce problème, il y a 2 solutions, soit vous récupérer des librairies d’implémentation et vous les ajouter dans le répertoire lib d’installation de votre Tomcat. Soit vous ajoutez la dépendance ci-dessous dans votre fichier pom :

```xml
<dependency>
     <groupId>javax.servlet</groupId>
     <artifactId>jstl</artifactId>
     <version>1.2</version>
</dependency>
```


