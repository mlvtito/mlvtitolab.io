+++ 
date = "2020-11-20"
title = "Récupérer les requêtes SQL de création d'une table existante"
tags = ["MySQL", "SQL"]
categories = []
+++

Si un jour, vous voulez retrouver rapidement la requête de création pour l'une de vos tables existante, il y a 2 possibilités. 

Soit vous faites un dump de la structure de votre base avec les ligne de commande qui vont bien. Soit, si vous êtes sous MySQL, vous passez la petite requête SQL ci-dessous :

```sql
show create table MY_AWESOME_TABLE;
```
