+++ 
date = "2012-07-30"
title = "Netbeans ne passera pas à côté d’Unity"
tags = []
categories = []
+++

Si parmis les IDE gratuits, comme moi, votre préférence se porte sur Netbeans (déjà je vous aimes bien car, d’une part, je n’en rencontre pas si souvent, et d’autre part, comme tout être humain, j’aime ceux qui me ressemble 😉 ). Et si comme moi, vous avez abandonné le système aux « fenêtres » pour quelque chose d’un peu plus underground comme Ubuntu, alors cet article va vous intéresser.

Vous n’êtes pas sans savoir que le bureau officiel d’Ubuntu est maintenant, et depuis quelques versions, Unity. Et vous avez sûrement remarqué que les menus des applications sont intégrés à la barre principale de l’OS (qui fait maintenant office de barre de titre des fenêtres, menu de la fenêtre, barre de notification et menu du système). Si vous avez bien poussé la porte de la 12.04, vous devriez pas avoir loupé le nouveau système de recherche indexée HUD qui permet de rechercher dans les menus des applications sans avoir à les retenir par cœur. Bref, c’est génial mais, ça ne fonctionne pas pour les applications Java comme Netbeans.

N’étant pas le premier Java Geek, d’autres sont passé avant moi pour intégrer la barre de menu de Netbeans à HUD. Et c’est Dan Jared qui, après avoir indiqué sur son blog comment installer son plugin Java Ayatana pour Netbeans (http://danjared.wordpress.com/netbeans/), semble avoir finalement distribuer ce plugin dans l’update center d’Oracle (http://plugins.netbeans.org/plugin/41822/java-ayatana).

![](../../assets/images/netbeans-ayatana-screenshot.png)

Pour rappel, Ayatana est le nom donné par la communauté Ubuntu pour unifier tous les efforts faits autour l’intégration d’Unity. Ca va du menu global dont on vient de parler dans le cas de Netbeans jusqu’au notifications (https://wiki.ubuntu.com/Ayatana). Notre ami maintient la librairie qui va vous permettre d’intégrer les menu Swing dans le menu global d’Unity : http://code.google.com/p/java-swing-ayatana/

