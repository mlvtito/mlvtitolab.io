+++ 
date = "2012-07-23"
title = "Git : Le commit qui fait mal"
tags = []
categories = []
+++

Imaginez-vous, un soir, vous êtes mal luné et vous remonté un fichier personnel qui vous a servi à faire vos tests dans votre gestionnaire de source. Les développements avancent. D’itération en itération, votre fichier personnel est trimbalé jusqu’au jour où arrive la livraison et que votre fichier personnel se retrouve dans le livrable de votre client.

Bon, c’est vrai, je pousse la caricature un peu loin. Si votre fichier est arrivé jusque là, c’est qu’il n’avais pas tant d’importance que ça ce fichier. Ou alors, vous vouliez parfaitement illustrer mon propos à vos collègues. Dans ce cas, je ne peux que vous en remercier.

Toujours est-il que la trace de votre fichier sera toujours visible dans ce satané commit. Et les futurs développeurs de votre projet (dont vous serez peut-être le chef dans 5-10 ans) sera toujours accessible. Heureusement, je viens de tomber sur la commande magique de Git :

```bash
git filter-branch --tree-filter 'rm -f my_file' HEAD
```

Le fichier my_file aura alors disparu de tous vos commits. Et la prochaine fois, vous éviterez de mélanger le boulot avec votre vie personnelle.

