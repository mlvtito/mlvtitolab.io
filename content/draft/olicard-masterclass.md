+++ 
date = "2019-02-28"
title = "Les Master Class de Fabien Olicard"
tags = []
categories = []
+++

### Les astuces

* Astuces pour le Calcul Mental

https://www.youtube.com/watch?v=8KVHpSFbuZs&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&t=0s&index=14

Jacow Trachenberg : https://fr.wikipedia.org/wiki/Méthode_Trachtenberg

* Connaitre tous les jours d'une année avec le Calendrier Perpétuel

https://www.youtube.com/watch?v=qLEESrIjcpE&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&t=0s&index=21

* L'Astuce pour connaitre et retenir ses tables de multiplications - Mental Vlog 27/366

https://www.youtube.com/watch?v=LrGbXOWBurk&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&t=0s&index=28

* L'Astuce de mémoire pour retenir les numéros de téléphone - Mental Vlog 34/366

https://www.youtube.com/watch?v=yFB6ROqkxFw&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&t=0s&index=35

* Astuce INCROYABLE, faire une multiplication avec un dessin - Mental Vlog 48/366

https://www.youtube.com/watch?v=vtCLUxRtAlo&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&index=49&t=0s

* Multiplier tous les nombres sans calculatrice - Astuce - 118/366

https://www.youtube.com/watch?v=vHs7vvmRTV8&index=119&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&t=0s

* PNL et VAKOG, la technique ultime pour mieux communiquer - Astuce - 125/366

https://www.youtube.com/watch?v=Yfz4btq49aQ&list=PLUD1bIh6qJDO3PdldY2XVnDZaPUyqU-xL&index=126&t=0s

## Les Master Class

* Comment avoir une mémoire incroyable - Master Class' - Mental Vlog 37/366

https://www.youtube.com/watch?v=eTKWahpW7c4

* Réussir son entretien d'embauche en 15 points - Master Class' - Mental Vlog 44/366

https://www.youtube.com/watch?v=oTjMWwp3UVU

* Compter et calculer au Boulier en seulement 3 secrets - Master Class' - Mental Vlog 51/366

https://www.youtube.com/watch?v=xeNbaB4GZ74

* Le palais mental, technique de mémoire - Master Class' - Mental Vlog 65/366

https://www.youtube.com/watch?v=cMGTLRgQQP0

* Apprendre 50% d'une langue en 5 minutes ! - Master Class' - Mental Vlog 72/366

https://www.youtube.com/watch?v=QdBQUxmkLhU

* Retenir un jeu de carte entier avec le système PAO - Master Class' - Mental Vlog 79/366

https://www.youtube.com/watch?v=UOZBYrqaOt8

* Techniques pour apprendre les verbes irréguliers - Master Class' - Mental Vlog 93/366

https://www.youtube.com/watch?v=zKyP53WBPkY


* Quels sont les bons livres et formations pour devenir mentaliste - Master Class - 107/366

https://www.youtube.com/watch?v=RKjYZmLWSag

* Apprendre un texte par coeur - Master Class - 114/366

https://www.youtube.com/watch?v=ltNelOgM1EQ

* Les intermittents du spectacle coûtent un milliard - Master Class' - 121/366

https://www.youtube.com/watch?v=aepsC3vRAJ0

* Réussir ses révisions et ses examens avec le Mind Map - Master Class' - 128/366

https://www.youtube.com/watch?v=NRIrL7rgEfs

* Devenez comme Sherlock avec le Cold Reading - Master Class' - 135/366

https://www.youtube.com/watch?v=NtjRgtGjgj0

* Comment faire des rêves lucides pour les contrôler - Master Class' - 142/366

https://www.youtube.com/watch?v=L0l539XvETw

* Les accords toltèques et le mentaliste - Master Class' - 149/366

https://www.youtube.com/watch?v=7xzo-UgfO70












