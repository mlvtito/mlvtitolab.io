+++ 
date = "2019-01-17"
title = "Dites m'en un peu plus sur le mail"
tags = []
categories = []
+++

## Coder l'envoi d'un message mail
To

Cc

Bcc

---

From

Reply-To

Return-Path

bounce address

## Quelques considération Ops

SPF : https://fr.wikipedia.org/wiki/Sender_Policy_Framework

DKIM : https://fr.wikipedia.org/wiki/DomainKeys_Identified_Mail

DMARC : https://fr.wikipedia.org/wiki/DMARC


## Glossaire

MTA : https://fr.wikipedia.org/wiki/Mail_Transfer_Agent

MTA-STS ?

https://fr.sendinblue.com/blog/spf-dkim-dmarc-delivrabilite-emails/

https://www.developpez.com/actu/255712/Gmail-devient-le-premier-fournisseur-principal-de-messagerie-a-prendre-en-charge-les-rapports-MTA-STS-et-TLS-pour-ameliorer-sa-securite/

