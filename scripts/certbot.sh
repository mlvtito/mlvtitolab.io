#!/bin/bash

# env.sh should contain
#   - CERTBOT_BASE_DIR for the certbot working dir
#   - GITLAB_TOKEN_HEADER containing the Gitlab private token to update the generated certificate
. ${HOME}/.rwx/blog/env.sh

certbot certonly --email arnaud.fonce@r-w-x.net --manual-public-ip-logging-ok --keep-until-expiring --non-interactive --agree-tos --work-dir ${CERTBOT_BASE_DIR}/var --config-dir ${CERTBOT_BASE_DIR}/etc --logs-dir ${CERTBOT_BASE_DIR}/logs --manual --manual-auth-hook scripts/gitlab-hugo-auth-hook.sh --manual-cleanup-hook scripts/gitlab-hugo-clean-hook.sh -d blog.r-w-x.net
