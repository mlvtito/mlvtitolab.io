#!/bin/bash

CERTBOT_DATA_DIR=/tmp/letsencrypt

certbot renew --email arnaud.fonce@r-w-x.net --manual-public-ip-logging-ok --keep-until-expiring --non-interactive --agree-tos --work-dir ${CERTBOT_DATA_DIR}/var --config-dir ${CERTBOT_DATA_DIR}/etc --logs-dir ${CERTBOT_DATA_DIR}/logs --manual --manual-auth-hook scripts/gitlab-hugo-auth-hook.sh --manual-cleanup-hook scripts/gitlab-hugo-clean-hook.sh -d blog.r-w-x.net
