#!/bin/bash

CURRENT_DIR=$(pwd)
cd /tmp/${CERTBOT_TOKEN}/mlvtito.gitlab.io
rm static/.well-known/acme-challenge/${CERTBOT_TOKEN}

git add .
git commit -m "letsencrypt challenge cleanup"
git push

cd ${CURRENT_DIR}
rm -rf /tmp/${CERTBOT_TOKEN}

$(dirname $0)/gitlab-pages-update-cert.sh


