#!/bin/bash

echo "Cloning the repository..."
mkdir -p /tmp/${CERTBOT_TOKEN}
cd /tmp/${CERTBOT_TOKEN}
git clone git@gitlab.com:mlvtito/mlvtito.gitlab.io.git
cd /tmp/${CERTBOT_TOKEN}/mlvtito.gitlab.io

echo "Committing the challenge file..."
mkdir -p static/.well-known/acme-challenge
echo ${CERTBOT_VALIDATION} > static/.well-known/acme-challenge/${CERTBOT_TOKEN}

git add .
git commit -m "letsencrypt challenge auth"
git push

echo "Waiting for the publication..."
sleep 2m

