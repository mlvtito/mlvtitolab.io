#!/bin/bash

. ${HOME}/.rwx/blog/env.sh

CERTBOT_LIVE_DIR="${CERTBOT_BASE_DIR}/etc/live/blog.r-w-x.net"

curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_HEADER}" "https://gitlab.com/api/v4/projects/9781499/pages/domains/blog.r-w-x.net" --form "key=@${CERTBOT_LIVE_DIR}/privkey.pem" --form "certificate=@${CERTBOT_LIVE_DIR}/fullchain.pem"

